# Rutenbeck homeassistant connector

This program connects
the [Rutenbeck TCR IP 4 Relays](https://www.rutenbeck.de/produkte/c/l/control/r-control-ip4-%28former-tcr-ip-4%29/r-control-ip4-%28former-tcr-ip-4%29)
to homeassistant via MQTT. The communication to the relays themselves is done via UDP.

*more description upcoming*