FROM python:3.8-slim

WORKDIR /app
COPY requirements.txt .
RUN python3.8 -m pip install -r requirements.txt

COPY RutenbeckBridge ./RutenbeckBridge
COPY main.py ./main.py

CMD python3.8 main.py
