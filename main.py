import logging
import time

from RutenbeckBridge.Bridge import MQTTBridge

logging.basicConfig(format="%(asctime)s: [%(levelname)s][%(name)s]: %(message)s", level=logging.INFO,
                    datefmt='%H:%M:%S')

logger = logging.getLogger("Bridge")

br = MQTTBridge('config.yaml')
logger.info("Started bridge")

try:
    while True:
        br.loop()
        time.sleep(.1)
except KeyboardInterrupt:
    br.stop()
    logger.info("Stopping bridge")
finally:
    del br
