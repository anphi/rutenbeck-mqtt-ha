"""
this file contains the main bridge class, handling all the work between
the components.
See comments and Class diagram for more information
"""
import logging
import sys
from datetime import datetime, timedelta
from typing import List

from ha_mqtt.ha_device import HaDevice
from ha_mqtt.mqtt_sensor import MqttSensor
from ha_mqtt.mqtt_switch import MqttSwitch, MqttDeviceSettings
from ha_mqtt.util import EntityCategory, HaDeviceClass
from paho.mqtt.client import Client

from RutenbeckBridge import Settings
from RutenbeckBridge.Rutenbeck import RutenbeckIP4
from RutenbeckBridge.UDPServer import UDPServer

logger = logging.getLogger("Bridge")


class RutenbeckMQTTBinding:
    def __init__(self, relay: RutenbeckIP4, channels: List[MqttSwitch], timestamp: MqttSensor):
        assert len(channels) == 4, "You must instantiate this class with exaclty 4 MQTT switches"
        self.relay = relay
        self.ch1 = channels[0]
        self.ch2 = channels[1]
        self.ch3 = channels[2]
        self.ch4 = channels[3]
        self.last_seen = datetime.now().astimezone()
        self.timestamp = timestamp
        self.offline_sent = False
        self.online_sent = False

    @property
    def mqtt_devices(self):
        return [self.ch1, self.ch2, self.ch3, self.ch4]


def id_from_name(name: str, channel: int) -> str:
    """
    calculates an id from the given channel name
    :param name: name to derive id from
    :param channel: channel number to derive the id from
    :return: derived ID
    """
    return f"{name.replace(' ', '')}-{channel + 1}"


class MQTTBridge:
    def __init__(self, config_file: str):
        try:
            self._settings = Settings.Settings(config_file)
        except FileNotFoundError:
            logger.fatal(f"config file {config_file} not found!")
            sys.exit(1)

        self.bindings = {}

        self.udp_server = UDPServer(self._settings.local_port)
        self.mqtt_client = Client("Rutenbeck MQTT Bridge")
        self.mqtt_client.username_pw_set(self._settings.mqtt_user, self._settings.mqtt_password)
        self.mqtt_client.connect(self._settings.mqtt_host, self._settings.mqtt_port)
        self.mqtt_client.loop_start()

        self.last_channel_check = datetime.now()

        for relay_conf in self._settings.relay_configurations:
            ha_device = HaDevice(relay_conf.friendly_name, relay_conf.unique_id)
            relay_dev = RutenbeckIP4(relay_conf.ip, relay_conf.port, self.udp_server.queue_tx_packet,
                                     self.state_change_callback, self.avail_change_callback)
            mqtt_devs = []
            mqtt_names = [relay_conf.ch1_name, relay_conf.ch2_name, relay_conf.ch3_name, relay_conf.ch4_name]
            # create mqtt devices for each channel
            for channel in range(4):
                name = mqtt_names[channel] if mqtt_names[channel] else f"Kanal {channel + 1}"
                settings = MqttDeviceSettings(name, f"{relay_conf.unique_id}-ch-{channel}", self.mqtt_client, ha_device)
                dev = MqttSwitch(settings)

                # make sure each lambda gets its own parameter,
                # otherwise all will have channel 4 as number
                dev.callback_on = lambda c=channel, r=relay_dev: r.turn_on(c + 1)
                dev.callback_off = lambda c=channel, r=relay_dev: r.turn_off(c + 1)

                mqtt_devs.append(dev)

            # create diagnostic device with timestamp
            timestamp_settings = MqttDeviceSettings(f"{relay_conf.name} last seen",
                                                    f"{relay_conf.name}-online",
                                                    self.mqtt_client, ha_device, EntityCategory.DIAGNOSTIC)
            timestamp = MqttSensor(timestamp_settings, "", HaDeviceClass.TIMESTAMP)
            timestamp.publish_state(datetime.now().astimezone().isoformat())

            self.bindings[relay_conf.ip] = RutenbeckMQTTBinding(relay_dev, mqtt_devs, timestamp)

        self.udp_server.start()
        self.query_relays()

    def query_relays(self):
        """
        queries all channels of all connected relays
        :return:
        """
        for relay in self.bindings.values():
            relay.relay.query()

    def stop(self):
        """
        stop all servers and threads
        :return:
        """

        for binding in self.bindings.values():
            for mqtt_dev in binding.mqtt_devices:
                mqtt_dev.send_offline()
            binding.timestamp.send_offline()

        self.udp_server.stop()
        self.mqtt_client.loop_stop()
        self.mqtt_client.disconnect()

    def loop(self):
        if not self.mqtt_client.is_connected():
            self.mqtt_client.reconnect()

        if not self.udp_server.rx_queue.empty():
            packet = self.udp_server.rx_queue.get()
            data = packet[1]
            addr = packet[0]

            if addr[0] in self.bindings:
                ip = addr[0]
                self.bindings.get(ip).relay.handle_incoming_udp_data(data)  # get by IP addresss
                self.bindings[ip].last_seen = datetime.now().astimezone()
                self.bindings[ip].timestamp.publish_state(self.bindings[ip].last_seen.isoformat())
            else:
                logger.warning(f"got message from unknown relay: {addr}")

        if datetime.now() - self.last_channel_check > timedelta(seconds=self._settings.query_every_seconds):
            self.query_relays()
            self.last_channel_check = datetime.now()

        for device in self.bindings.values():
            if datetime.now().astimezone() - device.last_seen > timedelta(
                    seconds=self._settings.query_every_seconds + 1):
                # device hasn't answered, send offline message to broker
                if not device.offline_sent:
                    for mqtt in device.mqtt_devices:
                        mqtt.send_offline()
                    device.offline_sent = True
                    device.online_sent = False

            else:
                # device is up
                if not device.online_sent:
                    for mqtt in device.mqtt_devices:
                        mqtt.send_online()
                    device.online_sent = True
                    device.offline_sent = False

    def state_change_callback(self, ip, channel, state):
        if channel and state is not None:
            self.bindings[ip].mqtt_devices[channel - 1].set(state)

    def avail_change_callback(self, ip, avail: bool):
        pass
