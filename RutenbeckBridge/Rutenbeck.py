"""
contains the class for the sending commands to the rutenbeck relay using UDP commands
"""
import logging
import re


class RutenbeckIP4:
    """
    rutenbeck relay holding 4 channels,
    communication via UDP
    """

    def __init__(self, ip, port, udp_send_function, state_change_callback, avail_change_callback):
        """
        creates an relay instance
        :param ip: IP Address of the relay
        :param port: port of the relay
        :param udp_send_function: function to call when the relay wants to send data
        :param state_change_callback: callback to call when the state of one of the channels switches
        :param avail_change_callback: callback to call when the online status switches
        """
        self.ip = ip
        self.port = port
        self.send_function = udp_send_function
        self.state_change_callback = state_change_callback
        self.avail_change_callback = avail_change_callback

    @staticmethod
    def create_datagram_data(channel: int, option: bool) -> bytes:
        """
        creates cmd string and encodes it as utf-8,
        ready to send to the relay
        :param channel: int, channel number in [1, 4]
        :param option: bool: whether to turn the channel on or off, 'True' for on, False for off
        :return: None
        """
        if option:
            option = 1
        else:
            option = 0
        assert type(channel) is int, "channel must be integer"
        assert 1 <= channel <= 4, "Only channels in interval [1, 4] are allowed"
        return "OUT{} {}".format(channel, option).encode('ascii')

    @staticmethod
    def parse_received_data(data: bytes) -> (int, bool):
        """
        parses binary data from a udp Datagram into a channel number and it's
        corresponding boolean value of the state
        :param data: bytes, binary datagram bytes
        :return: int: channel number, bool: state of the channel
        """
        reg = r"OUT([1-4])\s=([0-1])[\r\n]$"
        string = data.decode('utf-8')
        parse_result = re.match(reg, string)

        if parse_result is None or len(parse_result.groups()) < 2:
            return None, None

        state_str = parse_result.group(2)
        if state_str == '1':
            state = True
        else:
            state = False
        return int(parse_result.group(1)), state

    def handle_incoming_udp_data(self, data):
        """
        parses incoming UDP Data from ruthenbeck devices an decodes them into channel and state
        :param data: raw udp payload
        :return:
        """
        ch, state = RutenbeckIP4.parse_received_data(data)
        if ch is None or state is None:
            logging.warning(f"received invalid datagram {data}")
        self.state_change_callback(self.ip, ch, state)

    def turn_on(self, channel: int):
        data = RutenbeckIP4.create_datagram_data(channel, True)
        self.send_function(data, self.ip, self.port)
        self.query()

    def turn_off(self, channel: int):
        data = RutenbeckIP4.create_datagram_data(channel, False)
        self.send_function(data, self.ip, self.port)
        self.query()

    def switch(self, channel: int, state: bool):
        """
        switches the device according to the state
        :param channel: channel number to switch
        :param state: state to switch to
        :return:
        """

        if state:
            self.turn_on(channel)
        else:
            self.turn_off(channel)

    def query(self):
        """
        queries all output states from the device
        :return:
        """
        for i in range(1, 5):
            self.send_function(f"OUT{i} ?".encode('ascii'), self.ip, self.port)
