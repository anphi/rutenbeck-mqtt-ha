"""
little UDP device simulation useful for testing
"""
import socket
import sys
import threading

port = int(sys.argv[1])
print(f"starting up on port {port}")
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('localhost', port))
run = True


def udp_rx():
    while run:
        data, addr = sock.recvfrom(1024)
        print(f"got data from {addr}: {data}")


loop = threading.Thread(target=udp_rx)
loop.start()

try:
    while True:
        data = input("data to send: ")
        data = f"{data}\r\n"
        sock.sendto(data.encode('ascii'), ('localhost', 41000))

except KeyboardInterrupt:
    pass

finally:
    sock.sendto(b"0", ('localhost', port))
    sock.close()
    run = False
    loop.join()
