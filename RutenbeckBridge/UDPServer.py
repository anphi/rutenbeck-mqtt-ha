"""
contains the class with the low level UDP Server that
handles communication with the relays
"""
import errno
import logging
import socket
import threading
from queue import Queue
from time import sleep

logger = logging.getLogger("UDP Server")

DEFAULT_RELAY_PORT = 30303
LOCAL_PORT = 41000


class UDPServer:

    def __init__(self, local_port: int = 41000):
        """
        initializes the udp server and prepares it for sending and receiving
        :param local_port: port n the local machine to bind to
        """
        self.__port = local_port
        self.stop_flag = False
        self.no_data = False
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.rx_queue = Queue(maxsize=100)
        self.tx_queue = Queue(maxsize=100)
        self.sock.setblocking(False)
        try:
            self.sock.bind(('0.0.0.0', self.__port))
            logger.info(f"successfully bound to port {self.__port}")
        except OSError as e:
            logger.error(f"Failed to bind to port {self.__port}: {e}")

        self.runner = threading.Thread(target=self.__server_loop, name="udp server")

    def __del__(self):
        self.stop_flag = True
        self.sock.close()

    def queue_tx_packet(self, message: bytes, ip: str, port: int = DEFAULT_RELAY_PORT) -> None:
        """
        sends the given cmd string to the given IP via UDP datagram
        :param message: bytes, payload to send
        :param ip: str, target IP address
        :param port: port to send the packet to, default is the default port or the relays
        :return: Nones
        """
        self.tx_queue.put((message, ip, port))

    def stop(self):
        """
        sets the stop flag to true, server will quit on next opportunity
        :return: None
        """
        self.stop_flag = True
        self.runner.join()

    def start(self):
        """
        start the polling loop,
        queries UDP datagrams from the socket and calls the callback
        if the data is valid
        :return: None
        """
        self.runner.start()

    def __server_loop(self):
        """
        puts incoming packets in the rx_queue
        and sends packets in the tx_queue to it's destination
        :return:
        """
        while not self.stop_flag:
            # RX
            try:
                data, addr = self.sock.recvfrom(1024)
                if addr or data:
                    self.no_data = False
                    logger.debug(f"got message from {addr}: {data}")
                    self.rx_queue.put((addr, data))
            except socket.error as ex:
                if ex.args[0] == errno.EAGAIN or ex.args[0] == errno.EWOULDBLOCK:  # no data
                    self.no_data = True
                else:
                    raise ex

            # TX
            if not self.tx_queue.empty():
                self.no_data = False
                s, ip, port = self.tx_queue.get()
                logger.debug(f"sending data to: {ip}, {port}: {s}")
                self.sock.sendto(s, (ip, port))
            else:
                self.no_data = True

            # sleep if we didn't send or receive anything, otherwise continue directly in case
            # there are more packets queued.
            # Fixes delay when switching multiple devices at once
            if self.no_data:
                sleep(.1)
