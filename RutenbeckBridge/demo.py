import logging
import time

from RutenbeckBridge.Bridge import MQTTBridge

logging.basicConfig(format="%(asctime)s: [%(levelname)s][%(name)s]: %(message)s", level=logging.DEBUG,
                    datefmt='%H:%M:%S')

br = MQTTBridge('RutenbeckBridge/test_config.yaml')

try:
    while True:
        br.loop()
        time.sleep(.1)
except KeyboardInterrupt:
    br.stop()
finally:
    del br
