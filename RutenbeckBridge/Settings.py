import ipaddress

import yaml


class RelaySettings:
    def __init__(self):
        self.name = ""
        self.friendly_name = ""
        self.unique_id = ""
        self.ip = ""
        self.port = -1
        self.ch1_name = ""
        self.ch2_name = ""
        self.ch3_name = ""
        self.ch4_name = ""


class Settings:
    def __init__(self, path):
        config = yaml.safe_load(open(path, 'r'))
        if Settings.check_config(config):
            config = config['config']
            self.local_port = config.get("local_port")
            self.mqtt_host = config.get("mqtt_host")
            self.mqtt_port = config.get("mqtt_port")
            self.mqtt_user = config.get("mqtt_user", None)
            self.mqtt_password = config.get("mqtt_password", None)
            self.query_every_seconds = config.get("query_every_seconds")

            self.relay_configurations = []

            for relay in config.get("relays"):
                for name, relay_entry in relay.items():
                    relay_conf = RelaySettings()
                    relay_conf.name = name
                    relay_conf.friendly_name = relay_entry.get("friendly_name")
                    relay_conf.unique_id = relay_entry.get('unique_id')
                    relay_conf.ip = relay_entry.get("ip")
                    relay_conf.port = relay_entry.get("port")
                    relay_conf.ch1_name = relay_entry.get("ch1_name", "")
                    relay_conf.ch2_name = relay_entry.get("ch2_name", "")
                    relay_conf.ch3_name = relay_entry.get("ch3_name", "")
                    relay_conf.ch4_name = relay_entry.get("ch4_name", "")
                    self.relay_configurations.append(relay_conf)

    @staticmethod
    def check_config(settings: dict) -> bool:
        """
        checks if all required settings objects are present and contain valid values
        :param settings: settings dict to check
        :return: True, if valid
        """
        if not settings.get('config', None):
            raise ValueError("Top level key must be 'config")
        settings = settings['config']

        if type(settings) != dict:
            raise ValueError("the settings must form a dict")

        if not settings.get("local_port"):  # or type(settings.get('local_port')) != int:
            raise ValueError("There must be a local_port settings of type int")
        if not settings.get("query_every_seconds"):
            raise ValueError("Please specify how often the relays should be queried")

        relays = settings.get("relays")

        if len(relays) <= 0 or type(relays) != list:
            raise ValueError("There are no relays specified")

        for relay in relays:
            for name, relay_settings in relay.items():
                if type(relay_settings) != dict:
                    raise ValueError(f"you have to supply valid relay_settings for relay: {name}")
                if " " in name:
                    raise ValueError("The name must not contain spaces")
                if not relay_settings.get('friendly_name'):
                    raise ValueError(f"There must be an friendly display name for relay {name}")
                if not relay_settings.get('unique_id'):
                    raise ValueError(f"each relay needs an unique ID")
                if not relay_settings.get('ip') or not ipaddress.IPv4Address(relay_settings.get('ip')):
                    raise ValueError(f"the given IP address for relay {name} in invalid")
                if not relay_settings.get('port'):
                    raise ValueError(f"You need to give the port for relay {name}")
                if type(relay_settings.get('port')) != int:
                    raise ValueError(f"The port of relay {relay_settings} needs to be an integer")

        return True
